
_I hear and I forget, I see and I remember, I do and I understand._

## Introduction

utcp is a toy, and it's a simple userspace TCP implementation written in C.

READMD is also available in [中文版](/READMD-zh.md).

## What does utcp have

* TCP option MSS
* IP fragmentation reassembly
* ICMP reply 
* ARP reply


## Todo List


## Thanks

utcp took examples or even copied code from these amazing projects, thanks! 

- https://github.com/chobits/tapip.git
- https://github.com/rxi/log.c.git
- https://github.com/saminiir/level-ip.git

