![](./image/arpformat.png)



- Hardware Type：传输 ARP 报文的物理网络类型，最常见的是以太网类型，对应的值是 1。这个字段的长度是 2 字节

- Protocol Type：网络报文类型，字段长度是 2 字节。最常用的是 IPv4 报文，对应的值是 2048（十六进制为 0x0800，这和以太网帧中帧类型字段使用的 IP 报文类型相同，这是有意设计的）

- Hardware Address Length：物理地址长度，字段长度是 1 比特。ARP 协议报文中物理地址（MAC 地址）有多少比特，对于大部分网络来说，这个值是 6（因为 MAC 地址是 6 个字节，48 比特长）

- Protocol Address Length：网络地址长度，字段长度是 1 比特。表示 ARP 协议报文中网络地址（IP 地址）有多少比特，对于大部分网络来说，这个值是 4（因为 IPv4 地址是 4 个字节，32 比特）

- Opcode：ARP 报文的类型，接下来我们会看到几种不同的 ARP 报文。这个字段占用 2 个比特，值为 1 代表 ARP 请求报文，值为 2 代表 ARP 应答报文，3 代表 RARP 请求报文，4 代表 RARP 应答报文

- Sender Hardware Address：当前 ARP 报文的发送方的物理地址（MAC 地址）

- Send Protocol Address：当前 ARP 报文发送发的网络地址（IPv4 地址）

- Target Hardware Address：当前 ARP 报文接收方的物理地址（MAC 地址），如果是 ARP 情况请，这个字段为空（因为发送方就是不知道对方的 MAC 地址从会使用 ARP 来解析）

- Target Protocol Address：当前 ARP 报文接收方的网络地址（IPv4 地址）


