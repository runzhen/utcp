

一个 pcap 文件用于对照 [TCP_example.pcap](./TCP_example.pcap)

### 建立连接

三次握手的数据包

-  [SYN]       client 发出。seq = 0, ack = 0  (这里使用相对值)
-  [SYN, ACK]  server 发出。seq = 0, ack = 1
-  [ACK]       client 发出。seq = 1，ack = 1


结论： `三次握手不携带数据（wireshark中查看 tcp len 都是 0），但是消耗了一个序列号。`


在三次握手之后，tcp 链接已经建立起来了，之后  **B发回的包的 ack = A 上一个包的 seq + A 上一个包的 len** 



### 断开连接

- [FIN, ACK]  主动断开方，seq = 726, ack = 22951 (都等于上次的数字，没有消耗额外的序列号) 
- [FIN, ACK]  被动断开方，seq = 22951, ack = 727 (认为主动断开方的 FIN 消耗了一个字节)
- [ACK]       主动断开方，seq = 727， ack = 22952 （主动方认为被动方的 FIN 也消耗了一个字节）

总结： [FIN, ACK] 包会被对方认为消耗了一个字节，因此序列号加一。


这里只讨论了链接发起和断开最简单的情况，复杂情况后续再更新。


### 参考
- [http://packetlife.net/blog/2010/jun/7/understanding-tcp-sequence-acknowledgment-numbers/]()
