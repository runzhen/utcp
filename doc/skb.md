




sk_buff

成员 list 用于把这个 skb 挂载到某个链表上，等待后续的处理。这个链表可以是 IP 分片的 queue，也可以是 udp 的 receive queue。
同一时刻，同一个 skb 只会存在于一个 链表中，否则会产生严重错误。


