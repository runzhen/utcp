#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include "so.h"

int main()
{
    int sock = socket(AF_INET, SOCK_STREAM, 0);

    printf("sock = %d\n", sock);

    return 0;
}
