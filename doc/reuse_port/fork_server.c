#include <sys/types.h>  
#include <sys/socket.h>  
#include <netinet/in.h>  
#include <sys/wait.h>  
#include <stdio.h>  
#include <stdlib.h>  
#include <string.h>  
#include <unistd.h>  
#include <arpa/inet.h>

#define PROCESS_NUM 3 

int main(int argc, char *argv[])  
{  
    if (argc != 3) {
        printf("\n\t Usage: %s IP PORT", argv[0]);
        return -1;
    }

    int fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);  
    int connfd;  
    int pid, i;  
    char sendbuff[1024];  
    struct sockaddr_in serveraddr;  
    serveraddr.sin_family = AF_INET;  
    serveraddr.sin_addr.s_addr = inet_addr(argv[1]);  
    serveraddr.sin_port = htons(atoi(argv[2]));  
    bind(fd, (struct sockaddr*)&serveraddr, sizeof(serveraddr));  

    listen(fd, 1024);  

    for (i = 0; i < PROCESS_NUM; i++) {  
        int pid = fork();  
        if (pid == 0) {  
            while (1) {  
                connfd = accept(fd, (struct sockaddr*)NULL, NULL);  
                snprintf(sendbuff, sizeof(sendbuff), "accept PID is %d\n", getpid());  

                send(connfd, sendbuff, strlen(sendbuff) + 1, 0);  
                printf("process %d accept success!\n", getpid());  
                close(connfd);  
            }  
        }  
    }  

    int status;  
    wait(&status);  
    return 0;  
} 

