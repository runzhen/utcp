#ifndef __TCP_H__
#define __TCP_H__

#include "syshead.h"
#include "sock.h"
#include "list.h"
#include "ip.h"

struct tcphdr_s {
    uint16_t  sport;
    uint16_t  dport;
    uint32_t  seq;
    uint32_t  ack_seq;
    uint8_t   offset_rsvd; /* data offset and reserved */
    uint8_t   flags;
    uint16_t  win;
    uint16_t  csum;
    uint16_t  urp;
    uint8_t   data[]; /* the data includes Options, so 
                         ALLWAYS use tcp_data() to get the payload */
} __attribute__((packed));

typedef struct tcphdr_s tcphdr_t;

#define TCP_HDR_DEFAULT_LEN 20

#define FLAG_ACK    0x10
#define FLAG_RST    0x4
#define FLAG_SYN    0x2
#define FLAG_FIN    0x1

static inline uint8_t tcp_dataoffset(tcphdr_t *h)
{
    return ((h->offset_rsvd >> 4) * 4);
}

static inline bool_t tcp_flags_ack(tcphdr_t *h)
{
    return (h->flags & 0x10);
}

static inline bool_t tcp_flags_rst(tcphdr_t *h)
{
    return (h->flags & 0x4);
}

static inline bool_t tcp_flags_syn(tcphdr_t *h)
{
    return (h->flags & 0x2);
}

static inline bool_t tcp_flags_fin(tcphdr_t *h)
{
    return (h->flags & 0x1);
}

static inline uint8_t * tcp_options(tcphdr_t *h)
{
    return ((uint8_t *)h + 20);
}

static inline uint16_t tcp_options_len(tcphdr_t *h)
{
    return (tcp_dataoffset(h) - 20);
}

/* actually it is the same to ip_data() */
static inline tcphdr_t * tcp_hdr(sk_buff_t *skb)
{
    return (tcphdr_t *)ip_data(skb);
}

static inline uint8_t * tcp_data(sk_buff_t *skb)
{
    uint8_t * h = ip_data(skb);
    return (h + tcp_dataoffset((tcphdr_t *)h));
}

static inline uint16_t tcp_data_len(sk_buff_t *skb)
{
    tcphdr_t * h = tcp_hdr(skb);
    return (ip_data_len(ip_hdr(skb)) - tcp_dataoffset(h));
}

enum tcp_state {
    TCP_CLOSED = 1,
    TCP_LISTEN,
    TCP_SYN_RECV,
    TCP_SYN_SENT,
    TCP_ESTABLISHED,
    TCP_CLOSE_WAIT,
    TCP_LAST_ACK,
    TCP_FIN_WAIT1,
    TCP_FIN_WAIT2,
    TCP_CLOSING,
    TCP_TIME_WAIT,
    TCP_MAX_STATE
};

#define TCP_DEFAULT_WINDOW          4096
#define TCP_OPT_MSS_LEN             4
#define TCP_OPT_MSS                 1460
#define TCP_OPT_KIND_MSS            2

/* tcb, TCP control block */
struct tcb {
    struct list_head listen_queue;
    struct list_head accept_queue;
    struct list_head rcv_reass;

    /* transmission control block (RFC 793) */
    uint32_t snd_una;   /* send unacknowledged */
    uint32_t snd_nxt;   /* send next */
    uint32_t snd_wnd;   /* send window */
    uint32_t snd_up;    /* send urgent pointer */
    uint32_t snd_wl1;   /* seq for last window update */
    uint32_t snd_wl2;   /* ack for last window update */
    uint32_t iss;   /* initial send sequence number */
    uint32_t rcv_nxt;   /* receive next */
    uint32_t rcv_wnd;   /* receive window */
    uint32_t rcv_up;    /* receive urgent pointer */
    uint32_t irs;   /* initial receive sequence number */
    uint32_t state; /* connection state */
    uint16_t mss;
};

typedef struct tcb tcb_t;

tcb_t * tcp_alloc_tcb();


/* tcp_sock */
struct tcp_sock_s {
    uint32_t local;
    uint32_t peer;
    uint16_t lport;
    uint16_t pport;
    int32_t  fd;
    tcb_t    *tcb;
};
typedef struct tcp_sock_s tcp_sock_t;


/* functions */
void tcp_init();
void tcp_rcv(sk_buff_t *skb);
void tcp_send(tcp_sock_t *tcpsk, sk_buff_t *skb, tcphdr_t *tcph);

/* APIs */
void utcp_listen();
void utcp_socket();
void utcp_bind();
void utcp_accept();
void utcp_close(tcp_sock_t *tcpsk);
void utcp_connect(uint32_t peer, uint16_t pport);


#endif 
