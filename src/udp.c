#include "syshead.h"
#include "skbuff.h"
#include "arp.h"
#include "list.h"
#include "udp.h"
#include "sock.h"

#define UDP_SOCK_MAX   65536
typedef struct udp_sock_slot_s {
    struct hlist_head hash;
    pthread_mutex_t mutex;
} udp_sock_slot;

typedef struct udp_sock_table_s {
    udp_sock_slot  slot[UDP_SOCK_MAX];
    uint32_t       count;
} udp_sock_table;

udp_sock_table udp_sock_tbl;



sock_t * udp_lookup_sock(uint16_t dport)
{
    sock_t *sock = NULL;
    struct hlist_node *n = NULL;
    struct hlist_head *head = &udp_sock_tbl.slot[dport].hash;

    if (!head) {
        udp_debug("UDP sock table doesn't have this sock\n");
        return NULL;
    }

    hlist_for_each_entry_safe(sock, n, head, hash_list) {
        if (sock->addr.sport == ntohs(dport)) {
            return sock;
        }
    }

    return NULL;
}

int32_t udp_rcv(sk_buff_t *skb)
{
    sock_t *sock = NULL;
    udphdr_t *udph = (udphdr_t *)udp_hdr(skb);

    sock = udp_lookup_sock(udph->dport);
    if (!sock) {
        udp_debug("udp_lookup_sock() failed\n");
        goto drop;
    }
    list_add_tail(&skb->list, &sock->recv_queue);

    return 0;

drop:
    free_skb(skb);
    return -1;
}

void udp_init()
{
    int32_t i = 0; 
    for (i = 0; i < UDP_SOCK_MAX; i++) {
        hlist_head_init(&udp_sock_tbl.slot[i].hash);
    }

}
