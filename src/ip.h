#ifndef IPV4_H
#define IPV4_H 

#include "syshead.h"
#include "ethernet.h"
#include "skbuff.h"

#define IPV4   0x04
#define ICMPV4 0x01
#define IP_TCP 0x06
#define IP_UDP 0x11

#define IP_FLAG_RS  0x8000
#define IP_FLAG_DF  0x4000
#define IP_FLAG_MF  0x2000
#define IP_FLAG_ALL 0xE000
#define IP_FRAG_OFF 0x1FFF

#define IP_HDR_LEN 20

#ifdef CONFIG_DEBUG_IP
#define ip_hdr_debug(msg, hdr)                                                                \
    do {                                                                                      \
        if (hdr->daddr == 0xa000004) {                                                        \
            print_debug("ip "msg" (ihl: %hhu, version: %hhu, tos: %hhu, "                     \
                    "tot_len: %hu, id: %hu, flags: 0x%x, frag_offset: %hu, ttl: %hhu, "       \
                    "proto: %hhu, csum: %hx, "                                                \
                    "saddr: %hhu.%hhu.%hhu.%hhu, daddr: %hhu.%hhu.%hhu.%hhu)",                \
                    hdr->ihl,                                                                 \
                    hdr->version, hdr->tos, hdr->tot_len, hdr->id, ip_flags(hdr),             \
                    ip_offset(hdr), hdr->ttl, hdr->protocol, hdr->csum,                       \
                    hdr->saddr >> 24, hdr->saddr >> 16, hdr->saddr >> 8, hdr->saddr >> 0,     \
                    hdr->daddr >> 24, hdr->daddr >> 16, hdr->daddr >> 8, hdr->daddr >> 0);    \
        }                                                                                     \
    } while (0)

#define ip_debug(str, ...)   printf("%s:%u  "str, __FILE__, __LINE__, ##__VA_ARGS__);

#else 

#define ip_hdr_debug(msg, hdr)
#define ip_debug(str, ...)

#endif

#define __LITTLE_ENDIAN_BITFIELD

struct iphdr {
#if defined(__LITTLE_ENDIAN_BITFIELD)
    uint8_t   ihl:4,     /* mutiply 4 */
              version:4;
#elif defined (__BIG_ENDIAN_BITFIELD)
    uint8_t   version:4,
              ihl:4;
#else
#error  "Please fix <asm/byteorder.h>"
#endif
    uint8_t   tos;
    uint16_t  tot_len;
    uint16_t  id;
    uint16_t  flag_offset;  /* mutiply 8 */
    uint8_t   ttl;
    uint8_t   protocol;
    uint16_t  csum;
    uint32_t  saddr;
    uint32_t  daddr;
    /* The options start here */
    uint8_t data[];
} __attribute__((packed));

typedef struct iphdr iphdr_t;

struct ipfrag_hole_s {
    struct list_head next;
    int32_t left;
    int32_t right;
};

typedef struct ipfrag_hole_s ipfrag_hole_t;

struct ipfrag_s {
    uint8_t         protocol;
    uint8_t         done;
    uint16_t        n_skb;
    uint16_t        id;
    uint32_t        saddr;
    uint32_t        daddr;
    uint32_t        total_data_len; 
    struct list_head holes;  /* RFC 815 */
    struct list_head this_q;
    struct list_head all_q;
};

typedef struct ipfrag_s ipfrag_t;

static inline iphdr_t *ip_hdr(sk_buff_t *skb)
{
    return (iphdr_t *)(skb->head + ETH_HDR_LEN);
}

#define ip_hdr_len(iphdr) ((iphdr)->ihl << 2)

/* IP Header may has options, this function skips the options,
 * and return the actual payload start addres
 */
static inline uint8_t * ip_data(sk_buff_t *skb)
{
    iphdr_t *h = ip_hdr(skb);
    return (uint8_t *)(skb->head + ETH_HDR_LEN + (h->ihl << 2));
}

static inline int32_t ip_data_len(iphdr_t *h)
{
    return (h->tot_len - (h->ihl << 2));
}

#define ip_flags(hdr)         (((hdr)->flag_offset & IP_FLAG_ALL) >> 13)
#define ip_offset(hdr)        ((hdr)->flag_offset & IP_FRAG_OFF)
#define ip_offset_value(hdr)  ((ip_offset(hdr)) * 8)

void         ip_init();
void         ip_finish();
int32_t      ip_rcv(sk_buff_t *skb);
int32_t      ip_output(sk_buff_t *skb, uint32_t dst_addr);

#endif 
