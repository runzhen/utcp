#include "ethernet.h"
#include "icmpv4.h"
#include "ip.h"

void icmp_v4_rcv(sk_buff_t *skb)
{
    icmp_v4_t *icmp = (icmp_v4_t *)ip_data(skb);
    
    switch (icmp->type) {
    case ICMP_V4_ECHO:
        icmp_v4_reply(skb);
        return;
    case ICMP_V4_DST_UNREACHABLE:
        printf("ICMPv4 received 'dst unreachable' code %d\n", icmp->code);
        goto drop_pkt;
    default:
        printf("ICMPv4 did not match supported types\n");
        goto drop_pkt;
    }

drop_pkt:
    free_skb(skb);
    return;
}

void icmp_v4_reply(sk_buff_t *skb)
{
    iphdr_t *iphdr = ip_hdr(skb);
    icmp_v4_t *icmp = NULL;
    uint16_t icmp_len = ip_data_len(iphdr);

    skb_reserve(skb, ETH_HDR_LEN + ip_hdr_len(iphdr) + icmp_len);
    skb_push(skb, icmp_len);

    icmp = (icmp_v4_t *)skb->data;

    icmp->type = ICMP_V4_REPLY;
    icmp->csum = 0;
    icmp->csum = checksum(icmp, icmp_len, 0);

    skb->protocol = ICMPV4;

    if (ip_output(skb, iphdr->saddr) < 0) {
        printf("icmp: ip_output() failed\n");
    }
}
