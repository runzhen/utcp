#include "syshead.h"
#include "tuntap_if.h"
#include "timer.h"
#include "netdev.h"
#include "arp.h"
#include "ip.h"
#include "tcp.h"

enum {
    THREAD_TIMERS,
    THREAD_CORE,
    THREAD_APPS,
    THREAD_IPC,
    THREAD_SIGNAL,
    THREAD_MAX
};

static pthread_t threads[THREAD_MAX];

/* define some apps that use utcp */
typedef struct apps_s {
    char *name;
    void *(*func)(void *);
} apps_t;

static void *apps_passive_open_and_save(void *cmd)
{
    utcp_listen();
    return NULL;
}

static void *apps_active_open_and_save(void *input)
{
    char cmd[16], ip[32];
    uint32_t peer = 0;
    uint16_t port = 0;

    sscanf(input, "%s %s %hu", cmd, ip, &port);
    if (inet_pton(AF_INET, ip, &peer) != 1) {
        log_fatal("IP address is illegal\n");
        return NULL;
    }
    peer = ntohl(peer);
    log_debug("ip address 0x%x\n", peer);

    utcp_connect(peer, port);

    return NULL;
}

static void *apps_help()
{
    printf("help -- print help info\n");
    return NULL;
}

static void *apps_exit()
{
    exit(1);
    return NULL;
}

apps_t apps[] = {
    {"get", apps_active_open_and_save},
    {"help", apps_help},
    {"exit", apps_exit},
    {"passive", apps_passive_open_and_save}
}; 

/* end of apps */

static void create_thread(pthread_t id, void *(*func) (void *))
{
    if (pthread_create(&threads[id], NULL,
                       func, NULL) != 0) {
        printf("Could not create core thread\n");
    }
}

static void logo()
{
    printf("\n\tWelcome to utcp 0.1 \n\n");
    printf("- \"get\",  get <IP> <Port>\n");
    printf("- \"help\", help info\n");
    printf("- \"exit\", exit\n");
    printf("\n");
}

static void run_threads()
{
    printf("[+] utcp init ... \n");
    create_thread(THREAD_TIMERS, timers_start);
    create_thread(THREAD_CORE, netdev_rx_loop);
    printf("[+] init done. \n");
    logo();
}

#define shell_loop 1

static void start_shell()
{
    char user_input[128];
    char cmd[16];
    int32_t i, cmd_found;
    memset(cmd, 0, 16);

    while (shell_loop) {
        printf("utcp > ");
        cmd_found = 0;

        fgets(user_input, 128, stdin);
        sscanf(user_input, "%s ", cmd);

        for (i = 0; i < 4; i++) {
            if (!strcmp(cmd, apps[i].name)) {
                apps[i].func(user_input);
                cmd_found = 1;
            }
        }
        if (!cmd_found && (isalpha(cmd[0]) || isdigit(cmd[0]))) {
            printf("Command not found, try \"help\" ?\n");
        }
        memset(cmd, 0, 16);
    }
}

static void wait_for_threads()
{
    for (int i = 0; i < 2; i++) {
        if (pthread_join(threads[i], NULL) != 0) {
            printf("Error when joining threads\n");
        }
    }
}

static void init_log()
{
    FILE * log_file = fopen("/tmp/utcp.log", "w");
    if (!log_file) {
        printf("open log file failed\n");
        return;
    }
    log_set_fp(log_file);
    log_set_quiet(1);
}

static void init_stack()
{
    tun_init();
    netdev_init();
    arp_init();
    ip_init();
    tcp_init();
}

int main()
{
    init_log();
    init_stack();
    run_threads();
    start_shell();
    wait_for_threads();

    return 0;
}

