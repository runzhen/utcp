#ifndef __SOCKET_H__
#define __SOCKET_H__

#include "sock.h"
#include "skbuff.h"

typedef enum {
    SS_FREE = 0,
    SS_UNCONNECTED,
    SS_CONNECTING,
    SS_CONNECTED,
    SS_DISCONNECTING,
    SS_MAX
} socket_state;

enum {
    SOCKET_STREAM = 1,
    SOCKET_DGRAM,
    SOCKET_RAW,
    SOCKET_MAX
};

struct socket_s;

typedef struct socket_ops_s {
    int32_t (*socket)(struct socket_s *, int32_t);
    int32_t (*close)(struct socket_s *);
    int32_t (*accept)(struct socket_s *, struct socket_s *, sock_addr_t *);
    int32_t (*listen)(struct socket_s *, int32_t);
    int32_t (*bind)(struct socket_s *, sock_addr_t *);
    int32_t (*connect)(struct socket_s *, sock_addr_t *);
    int32_t (*read)(struct socket_s *, void *, int32_t);
    int32_t (*write)(struct socket_s *, void *, int32_t);
    int32_t (*send)(struct socket_s *, void *, int32_t, sock_addr_t *);
    sk_buff_t *(*recv)(struct socket_s);
} socket_ops_t;

typedef struct socket_s {
    socket_state        state;
    int32_t             type;
    socket_ops_t        *ops;
    sock_t              *sock;
} socket_t;

#endif
