#include "syshead.h"
#include "skbuff.h"
#include "arp.h"
#include "ip.h"
#include "list.h"
#include "icmpv4.h"
#include "udp.h"
#include "tcp.h"

static LIST_HEAD(ipfrag_queue); /* ip fragmentation queue */

#if defined(CONFIG_DEBUG_IP) || defined(CONFIG_DEBUG_TCP)
void ip_dump_hdr_egress(sk_buff_t *skb, iphdr_t *iph)
{
    /* IP header */
    log_printf("%-8s%hhu.%hhu.%hhu.%hhu->%hhu.%hhu.%hhu.%hhu, protocol %hhu\n", 
           "IP:",
           iph->saddr >> 24, iph->saddr >> 16, iph->saddr >> 8, iph->saddr >> 0,
           iph->daddr >> 24, iph->daddr >> 16, iph->daddr >> 8, iph->daddr >> 0,
           iph->protocol);
    log_printf("%-8sversion %hhu, ihl %hhu, tos 0x%hx, len %hhu\n",
           " ", iph->version, iph->ihl, iph->tos, iph->tot_len);
    log_printf("%-8sid %hhu, frag_off 0x%hx, ttl %hhu, checksum %hu(0x%hx)\n",
           " ", iph->id, ip_offset(iph), iph->ttl, iph->csum, iph->csum);
    log_printf("%-8sskb->len = %d\n", " ", skb->len);
}
#else 
void ip_dump_hdr_egress(sk_buff_t *skb, iphdr_t *iph){};
#endif

static sk_buff_t *ip_reass(sk_buff_t *skb);

static int32_t ip_hdr_sanity_check(iphdr_t *hdr)
{
    uint16_t csum = -1;
    if (hdr->version != IPV4) {
        log_debug("Datagram version was not IPv4\n");
        goto drop;
    }
    if (hdr->ihl < 5) {
        log_debug("IPv4 header length must be at least 5\n");
        goto drop;
    }
    if (hdr->ttl == 0) {
        log_debug("Time to live of datagram reached 0\n");
        goto drop;
    }
    csum = checksum(hdr, hdr->ihl * 4, 0);
    if (csum != 0) {
        log_debug("IP header checksum mis-match\n");
        goto drop;
    }
    return 0;

drop:
    return -1;
}

static inline void ip_header_ntoh(sk_buff_t *skb)
{
    iphdr_t *hdr = ip_hdr(skb);

    hdr->tot_len = ntohs(hdr->tot_len);
    hdr->id = ntohs(hdr->id);
    hdr->flag_offset = ntohs(hdr->flag_offset);
    hdr->saddr = ntohl(hdr->saddr);
    hdr->daddr = ntohl(hdr->daddr);
    hdr->csum = ntohs(hdr->csum);
}

#if defined(CONFIG_DEBUG_IP) && !defined(CONFIG_DEBUG_TCP)
void ip_dump_header(sk_buff_t *skb)
{
    iphdr_t  *iph = ip_hdr(skb);
    /* IP header */
    log_printf("%-8s%hhu.%hhu.%hhu.%hhu->%hhu.%hhu.%hhu.%hhu, protocol %hhu\n",
           "IP:",
           iph->saddr >> 24, iph->saddr >> 16, iph->saddr >> 8, iph->saddr >> 0,
           iph->daddr >> 24, iph->daddr >> 16, iph->daddr >> 8, iph->daddr >> 0,
           iph->protocol);
    log_printf("%-8sversion %hhu, ihl %hhu, tos 0x%hx, len %hhu\n",
           " ", iph->version, iph->ihl, iph->tos, iph->tot_len);
    log_printf("%-8sid %hhu, frag_off 0x%hx, ttl %hhu, checksum %hu(0x%hx)\n",
           " ", iph->id, ip_offset(iph), iph->ttl, iph->csum, iph->csum);
    log_printf("\n\n");
}
#else 
void ip_dump_header(sk_buff_t *skb) {}
#endif

int32_t ip_rcv(sk_buff_t *skb)
{
    iphdr_t *hdr = ip_hdr(skb);
    sk_buff_t *skb_new = NULL;
    int32_t  ret = 0;

    /* put the sanity check before ip_header_ntoh()
     * because we rcv a lot of strange packets from ip_rcv(), 
     * which cause coredump if htoh() before sanity check 
     * */
    if (ip_hdr_sanity_check(hdr) < 0) {
        goto drop_pkt;
    }

    ip_header_ntoh(skb);
    ip_dump_header(skb);
    
    /* IP fragmentation */
    if (hdr->flag_offset & (IP_FRAG_OFF | IP_FLAG_MF)) {
        if (hdr->flag_offset & IP_FLAG_DF) {
            log_debug("Both MF and DF are set, drop it!\n");
            ret = -1;
            goto drop_pkt;
        }
        skb_new = ip_reass(skb);
        if (!skb_new) {
            return ret;
        } else {
            hdr = ip_hdr(skb_new);
        }
    }
    if (!skb_new) {
        skb_new = skb;
    }

    switch (hdr->protocol) {
    case ICMPV4:
        icmp_v4_rcv(skb_new);
        return 0;
    case IP_UDP:
        udp_rcv(skb_new);
        return 0;
    case IP_TCP:
        tcp_rcv(skb_new);
        return 0;
    default:
        goto drop_pkt;
        //ip_output(skb, hdr->saddr);
    }

drop_pkt:
    //free_skb(skb);
    free_skb(skb_new);
    return ret;
}

static ipfrag_t *ipfrag_lookup(iphdr_t *iph)
{
    ipfrag_t * f = NULL;

    list_for_each_entry(f, &ipfrag_queue, all_q) {
        if (f->protocol == iph->protocol &&
            f->id == iph->id && 
            f->saddr == iph->saddr &&
            f->daddr == iph->daddr) {

           return f;
        }
    } 
    return NULL;
}

#define IPFRAG_RIGHT_MAX    65535

static ipfrag_t *ip_new_frag(iphdr_t *iph)
{
    ipfrag_hole_t *t = NULL;
    ipfrag_t * f = (ipfrag_t *)malloc(sizeof(ipfrag_t));
    memset(f, 0, sizeof(ipfrag_t));

    f->protocol = iph->protocol;
    f->id = iph->id;
    f->saddr = iph->saddr;
    f->daddr = iph->daddr;

    list_add(&f->all_q, &ipfrag_queue);
    list_init(&f->this_q);
    list_init(&f->holes);

    /*
     *  RFC 815, by default there is one "hole" 
     *      _________            _________
     *  ... _________|          |_________ ....
     *               0          65536   
     */
    t = (ipfrag_hole_t *)malloc(sizeof(ipfrag_hole_t));
    memset(t, 0, sizeof(ipfrag_hole_t));
    t->left = 0; 
    t->right = IPFRAG_RIGHT_MAX;
    list_add_tail(&t->next, &f->holes);

    return f;
}

#if defined(CONFIG_DEBUG_IP)
static void ipfrag_dump_holes(ipfrag_t *frag)
{
    ipfrag_hole_t *hole = NULL;
    int32_t count = 1;

    list_for_each_entry(hole, &frag->holes, next) {
        log_debug("%d : [%d, %d]\n", count, hole->left, hole->right);
    }
}
#else 
static void ipfrag_dump_holes(ipfrag_t *frag){}
#endif

static int32_t ipfrag_insert(sk_buff_t *skb, ipfrag_t *frag)
{
    iphdr_t *iph = ip_hdr(skb);
    ipfrag_hole_t *hole = NULL, *new_hole = NULL;
    int32_t iph_left = 0, iph_right = 0;

    iph_left = ip_offset_value(iph);
    iph_right = iph_left + ip_data_len(iph) - 1;

    /* TODO can't handle the frags overlapping senario */

    /* Follow RFC 815 */
    list_for_each_entry(hole, &frag->holes, next) {
        if ((iph_left > hole->right) || (iph_right < hole->left)) {
            continue;
        }

        /* handle last frag, MF = 0 but offset != 0 */
        if ((iph->flag_offset & IP_FLAG_MF) == 0) {
            frag->total_data_len = ip_offset_value(iph) + ip_data_len(iph);
            hole->right = iph_right;
        }

        ip_debug("hole: [%d, %d], ip: [%d, %d]\n", 
                 hole->left, hole->right, iph_left, iph_right);

        if ((iph_left > hole->left) && (iph_right == hole->right)) {
            new_hole = (ipfrag_hole_t *)malloc(sizeof(ipfrag_hole_t));
            new_hole->left = hole->left;
            new_hole->right = iph_left - 1;

            list_add_tail(&new_hole->next, &hole->next);
            list_del(&hole->next);
            
            goto hole_done;
        } else if ((iph_left == hole->left) && (iph_right < hole->right)) {
            new_hole = (ipfrag_hole_t *)malloc(sizeof(ipfrag_hole_t));
            new_hole->left = iph_right + 1;
            new_hole->right = hole->right;

            list_add_tail(&new_hole->next, &hole->next);
            list_del(&hole->next);

            goto hole_done;
        } else if ((iph_left > hole->left) && (iph_right < hole->right)) {
            /* 1st hole */
            new_hole = (ipfrag_hole_t *)malloc(sizeof(ipfrag_hole_t));
            new_hole->left = hole->left;
            new_hole->right = iph_left - 1;

            list_add_tail(&new_hole->next, &hole->next);

            /* 2nd hole */
            new_hole = (ipfrag_hole_t *)malloc(sizeof(ipfrag_hole_t));
            new_hole->left = iph_right + 1;
            new_hole->right = hole->right;

            list_add(&new_hole->next, &hole->next);
            list_del(&hole->next);

            goto hole_done;
        } else if ((iph_left == hole->left) && (iph_right == hole->right)) {
            /* delete this hole */
            list_del(&hole->next);
            goto hole_done;
        } else {
            log_debug("we don't support IP frag overlapping\n");
            /* drop this packet */
            free_skb(skb);
            return -1;
        }
    }

hole_done:
    if (hole) {
        free(hole);
        hole = NULL;
    }

    ipfrag_dump_holes(frag);

    list_add(&skb->list, &frag->this_q);
    frag->n_skb++;

    if (list_empty(&frag->holes)) {
        frag->done = 1;
    }

    return 0;
}

static sk_buff_t * ipfrag_combine(ipfrag_t *frag)
{
    sk_buff_t *pos = NULL, *new_skb = NULL;
    int32_t offset = 0, counter = 0, skb_buffer_len = 0;
    iphdr_t *iph = NULL, *new_iph = NULL;
    uint8_t skb_data[frag->total_data_len];

    /* get first entry, for ETH and IP header */
    pos = list_first_entry(&frag->this_q, sk_buff_t, list);
    new_iph = ip_hdr(pos);
    skb_buffer_len = ETH_HDR_LEN + ip_hdr_len(new_iph) + frag->total_data_len;

    /* FIXME it is dangerous */
    new_skb = alloc_skb(skb_buffer_len);
    new_skb->len = skb_buffer_len;

    memcpy(new_skb->head, pos->head, (ETH_HDR_LEN + ip_hdr_len(new_iph)));
    new_skb->data += (ETH_HDR_LEN + ip_hdr_len(new_iph));

    while (!list_empty(&frag->this_q)) {
        list_for_each_entry(pos, &frag->this_q, list) {
            iph = ip_hdr(pos);

            if (offset == ip_offset_value(iph)) {
                /* found ! */
                memcpy(&skb_data[offset], ip_data(pos), (ip_data_len(iph)));
                offset += ip_data_len(iph);
                list_del(&pos->list);
                free_skb(pos);
                break;
            }
        }
        counter++;
        if (counter > 65536) {
            log_debug("break from infinity loop\n");
            goto error;
        }
    }

    memcpy(new_skb->data, skb_data, frag->total_data_len);
    return new_skb;

error:
    return NULL;
}

static sk_buff_t *ip_reass(sk_buff_t *skb)
{
    iphdr_t *iph = ip_hdr(skb);
    ipfrag_t *frag = NULL;
    sk_buff_t *skb_new = NULL;

    frag = ipfrag_lookup(iph);
    if (frag == NULL) {
        frag = ip_new_frag(iph);
    }
    if (ipfrag_insert(skb, frag) < 0) {
        log_debug("ipfrag_insert() failed\n");
        return NULL;
    }
    if (frag->done) {
        skb_new = ipfrag_combine(frag);
        if (skb_new) {
            list_del(&frag->all_q);
        }
    }

    return skb_new;
}

int32_t ip_output(sk_buff_t *skb, uint32_t dst_addr)
{
    iphdr_t *ihdr = ip_hdr(skb);
    uint32_t csum = 0;
    int32_t  ret = 0;
    uint8_t mac[6];
    uint8_t *src = NULL;

    /* TODO routing table lookup, here just hard code */

    /* currently we don't support IP option 
     * so, IP header is always 20 
     */
    skb_push(skb, IP_HDR_LEN);

    ihdr->version = IPV4;
    ihdr->ihl = 0x05;
    ihdr->tos = 0;
    ihdr->tot_len = skb->len;
    ihdr->id = 0xa;
    ihdr->flag_offset = 0;
    ihdr->ttl = 64;
    ihdr->protocol = skb->protocol;
    /* some protocol, such as ICMP don't have sock */
    if (skb->sock) {
        ihdr->saddr = skb->sock->local;
    } else {
        ihdr->saddr = skb->dev->addr;
    }
    ihdr->daddr = dst_addr;
    ihdr->csum = 0;
    ihdr->flag_offset = htons(ihdr->flag_offset);

    ip_hdr_debug("out", ihdr);

    src = arp_get_mac_by_ip(dst_addr);

    if (src) {
        memcpy(mac, src, 6);
    }

    /* dump */
    ip_dump_hdr_egress(skb, ihdr);
    /* end dump */

    ihdr->tot_len = htons(ihdr->tot_len);
    ihdr->id = htons(ihdr->id);
    ihdr->daddr = htonl(ihdr->daddr);
    ihdr->saddr = htonl(ihdr->saddr);

    csum = checksum(ihdr, ihdr->ihl * 4, 0);
    ihdr->csum = csum;
    
    /* TODO arp lookup */
    netdev_transmit(skb, mac, ETH_P_IP);

    return ret;
}

void ip_init()
{
}

void ip_finish()
{
    /* TODO */
}

