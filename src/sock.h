#ifndef __SOCK_H__
#define __SOCK_H__

#include "skbuff.h"

struct sock_addr_s {
    uint32_t src;
    uint32_t dst;
    uint16_t sport;
    uint16_t dport;
} __attribute__((packed));
typedef struct sock_addr_s sock_addr_t;


struct sock_s;
struct sock_ops_s {
    void (*recv_notify)(struct sock_s *);
    void (*send_notify)(struct sock_s *);
    int (*send_pkb)(struct sock_s *, sk_buff_t *);
    int (*bind)(struct sock_s *, sock_addr_t *);
    int (*connect)(struct sock_s *, sock_addr_t *);
    int (*close)(struct sock_s *);
    int (*listen)(struct sock_s *, int);
    struct sock_s *(*accept)(struct sock_s *);
};
typedef struct sock_ops_s sock_ops_t;


struct sock_s {
    unsigned char           proto;
    struct sock_ops_t       *ops;
    sock_addr_t             addr;
    struct list_head        recv_queue;
    struct hlist_node       hash_list;
    uint32_t                ref;
} __attribute__((packed));
typedef struct sock_s sock_t;

#endif
