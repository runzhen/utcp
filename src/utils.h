#ifndef UTILS_H
#define UTILS_H

#define CMDBUFLEN 100

#define print_debug(str, ...)                       \
    printf(str" - %s:%u\n", ##__VA_ARGS__, __FILE__, __LINE__);

#define print_err(str, ...)                     \
    fprintf(stderr, str, ##__VA_ARGS__);



/* compare the two seq number, 
 * copied from Linux kernel include/net/tcp.h 
 */

/*
 * The next routines deal with comparing 32 bit unsigned ints
 * and worry about wraparound (automatic with unsigned arithmetic).
 */

static inline int32_t before(uint32_t seq1, uint32_t seq2)
{
        return (int32_t)(seq1-seq2) < 0;
}
#define after(seq2, seq1)   before(seq1, seq2)

/* is s2<=s1<=s3 ? */
static inline int32_t between(uint32_t seq1, uint32_t seq2, uint32_t seq3)
{
    return seq3 - seq2 >= seq1 - seq2;
}


void     printb(char *buf, uint32_t size);

uint32_t sum_every_16bits(void *addr, uint16_t count);
uint16_t checksum(void *addr, uint16_t count, uint32_t start_sum);

int      run_cmd(char *cmd, ...);
int      get_address(char *host, char *port, struct sockaddr *addr);
uint32_t parse_ipv4_string(char *addr);
uint32_t min(uint32_t x, uint32_t y);

uint16_t tcp_checksum(uint32_t src, uint32_t dst, uint16_t proto,
             uint16_t len, uint8_t *data);

#endif
