#ifndef __UDP_H__
#define __UDP_H__

#include "syshead.h"
#include "ip.h"

struct udphdr_s {
    uint16_t sport; /* source port */
    uint16_t dport; /* destination port */
    uint16_t length;  /* udp head and data */
    uint16_t checksum;
    uint8_t  data[0];
} __attribute__((packed));

typedef struct udphdr_s udphdr_t;

#define udp_hdr_len       sizeof(udphdr_t)
#define udp_hdr(skb)      (ip_data(skb))
#define udp_len(udph)     (ntohs(udph->length))


#ifdef CONFIG_DEBUG_UDP
#define udp_debug(str, ...)   printf("%s:%u  "str, __FILE__, __LINE__, ##__VA_ARGS__);
#else
#define udp_hdr_debug(msg, hdr)
#define udp_debug(str, ...)

#endif

int32_t udp_rcv(sk_buff_t *skb);

#endif
