#include "arp.h"
#include "netdev.h"
#include "skbuff.h"
#include "list.h"

arp_cache_entry_t *cache = NULL;

void arp_add_cache_entry(uint32_t ip, uint8_t * mac)
{
    cache->sip = ip;
    memcpy(cache->smac, mac, 6);
}

uint8_t * arp_get_mac_by_ip(uint32_t ip)
{
    return cache->smac;

    printf("Error: can't find MAC addr by ip\n");
    return NULL;
}

static inline void arp_copy_mac(void *dst, void *src)
{
    memcpy(dst, src, 6);
}

void arp_request()
{
    uint32_t size = ETH_HDR_LEN + ARP_HDR_LEN + ARP_DATA_LEN;
    sk_buff_t *skb = alloc_skb(size);
    skb_reserve(skb, size);

    skb_push(skb, ARP_HDR_LEN + ARP_DATA_LEN);

    arp_hdr_t *arphdr = arp_hdr(skb);
    arphdr->hwtype = htons(ARP_ETHERNET);
    arphdr->protype = htons(ARP_IPV4);
    arphdr->hwsize = 6;
    arphdr->prosize = 4;
    arphdr->opcode = htons(ARP_REQUEST);

    arp_ipv4_t *arpdata = (arp_ipv4_t *)arphdr->data;
    netdev_t *netdev = netdev_get(0);

    arpdata->sip = htonl(netdev->addr);
    arp_copy_mac(arpdata->smac, netdev->hwaddr);

    arpdata->dip = htonl(0xA000005);
    arpdata->dmac[0] = 0xFF;
    arpdata->dmac[1] = 0xFF;
    arpdata->dmac[2] = 0xFF;
    arpdata->dmac[3] = 0xFF;
    arpdata->dmac[4] = 0xFF;
    arpdata->dmac[5] = 0xFF;

    skb->dev = netdev;

    if (netdev_transmit(skb, arpdata->dmac, ETH_P_ARP) < 0) {
        printf("netdev_transmit() error\n");
    }
}

void arp_init()
{
    cache = (arp_cache_entry_t *)malloc(sizeof(arp_cache_entry_t));

    /* At beginning of TCP SYN, the arp cache is empty,
     * so the workaround is to send an ARP_REQUEST in arp_init().
     * In the long term, should do it when arp_get_mac_by_ip() fails.
     */
    arp_request();
}

void
arp_rcv(sk_buff_t *skb)
{
    arp_hdr_t *arphdr = NULL;
    arp_ipv4_t *arpdata = NULL;
    netdev_t *netdev = NULL;

    arphdr = arp_hdr(skb);

    /* parse received skb arp header */
    arphdr->hwtype = ntohs(arphdr->hwtype);
    arphdr->protype = ntohs(arphdr->protype);
    arphdr->opcode = ntohs(arphdr->opcode);

    arp_debug("rx", arphdr);

    /* parse received skb arp data */
    arpdata = (arp_ipv4_t *) arphdr->data;

    arpdata->sip = ntohl(arpdata->sip);
    arpdata->dip = ntohl(arpdata->dip);

    arpdata_debug("rx", arpdata);
    
    /* TODO add to cache */
    arp_add_cache_entry(arpdata->sip, (uint8_t *)arpdata->smac);

    /* lookup for dst IP */
    if (!(netdev = netdev_get(arpdata->dip))) {
        goto drop_pkt;
    }

    switch (arphdr->opcode) {
    case ARP_REQUEST:
        arp_reply(skb, netdev);
        break;
    case ARP_REPLY:
        log_debug("ARP reply is added to cache\n");
        break;
    default:
        printf("ARP: Opcode not supported\n");
        goto drop_pkt;
    }

    /* everything is OK, return */
    return;

drop_pkt:
    if (skb) {
        free_skb(skb);
    }
}

void 
arp_reply(sk_buff_t *skb, netdev_t *netdev)
{
    arp_hdr_t *arphdr = NULL;
    arp_ipv4_t *arpdata = NULL;

    arphdr = arp_hdr(skb);

    skb_reserve(skb, ETH_HDR_LEN + ARP_HDR_LEN + ARP_DATA_LEN);
    skb_push(skb, ARP_HDR_LEN + ARP_DATA_LEN);

    arpdata = (arp_ipv4_t *)arphdr->data;

    memcpy(arpdata->dmac, arpdata->smac, 6);
    arpdata->dip = arpdata->sip;

    memcpy(arpdata->smac, netdev->hwaddr, 6);
    arpdata->sip = netdev->addr;

    arphdr->opcode = ARP_REPLY;

    arp_debug("reply", arphdr);

    arphdr->opcode = htons(arphdr->opcode);
    arphdr->hwtype = htons(arphdr->hwtype);
    arphdr->protype = htons(arphdr->protype);

    arpdata_debug("reply", arpdata);
    arpdata->sip = htonl(arpdata->sip);
    arpdata->dip = htonl(arpdata->dip);

    skb->dev = netdev;

    if (netdev_transmit(skb, arpdata->dmac, ETH_P_ARP) < 0) {
        printf("netdev_transmit() failed\n");
    }
}
