#include "syshead.h"
#include "netdev.h"
#include "skbuff.h"
#include "tuntap_if.h"
#include "arp.h"
#include "ip.h"

netdev_t *netdev = NULL;

netdev_t *
netdev_alloc(char *addr, char *hwaddr, uint32_t mtu)
{
    netdev_t *dev = (netdev_t *)malloc(sizeof(netdev_t));

    if (inet_pton(AF_INET, addr, &dev->addr) != 1) {
        printf("inet_pton() failed\n");
        return 0;
    }

    dev->addr = ntohl(dev->addr);

    sscanf(hwaddr, "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", 
           &dev->hwaddr[0], &dev->hwaddr[1], &dev->hwaddr[2],
           &dev->hwaddr[3], &dev->hwaddr[4], &dev->hwaddr[5]);

    dev->addr_len = 6;
    dev->mtu = mtu;

    return dev;
}

void 
netdev_init()
{
    netdev = netdev_alloc("10.0.0.4", "00:0c:21:22:23:24", 1500);
}

int 
netdev_transmit(sk_buff_t *skb, uint8_t *dst_hw, uint16_t ethertype)
{
    eth_hdr_t *ehdr = NULL;
    int ret = 0;

    skb_push(skb, ETH_HDR_LEN);

    ehdr = (eth_hdr_t *)skb_head(skb);

    memcpy(ehdr->dmac, dst_hw, netdev->addr_len);
    memcpy(ehdr->smac, netdev->hwaddr, netdev->addr_len);

    ehdr->ethertype = htons(ethertype);

    log_printf("%-8sbefore skb(%p) going out, len = %d\n", "Eth:", skb, skb->len);
    ret = tun_write((char *)skb->data, skb->len);

    free_skb(skb);

    return ret;
}

static int
netdev_receive(sk_buff_t *skb)
{
    struct eth_hdr *hdr = eth_hdr(skb);
    switch (hdr->ethertype) {
    case ETH_P_IP:
        ip_rcv(skb);
        break;
    case ETH_P_ARP:
        arp_rcv(skb);
        break;
    default:
        free_skb(skb);
    }
    return 0;
}

void *
netdev_rx_loop()
{
    while (1) {
        sk_buff_t *skb = alloc_skb(BUFLEN);
        if (tun_read((char *)skb->data, BUFLEN) < 0) {
            printf("tun_read() failed\n");
            free_skb(skb);
            return NULL;
        }
        netdev_receive(skb);
    }
    return NULL;
}

netdev_t *
netdev_get(uint32_t sip)
{
    if (netdev->addr == sip) {
        return netdev;
    }

    /* FIXME we only have one dev */
    return netdev;
}

void 
free_netdev()
{
    free(netdev);
}

