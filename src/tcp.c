#include "syshead.h"
#include "skbuff.h"
#include "arp.h"
#include "ethernet.h"
#include "ip.h"
#include "list.h"
#include "tcp.h"
#include "utils.h"
#include <sys/time.h>

/* global varibles */
vector global_tcp_sock;

/* end */

void tcp_init()
{
    global_tcp_sock = vector_init(sizeof(tcp_sock_t *));
}

const char *tcp_state_string[TCP_MAX_STATE] = {
    "Unknown tcp state: 0",
    "CLOSED",
    "LISTEN",
    "SYN-RECV",
    "SYN-SENT",
    "ESTABLISHED",
    "CLOSE-WAIT",
    "LAST-ACK",
    "FIN-WAIT-1",
    "FIN-WAIT-2",
    "CLOSING",
    "TIME-WAIT",
};

/*
0                   1                   2                   3
0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|          Source Port          |       Destination Port        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                        Sequence Number                        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                    Acknowledgment Number                      |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|  Data |           |U|A|P|R|S|F|                               |
| Offset| Reserved  |R|C|S|S|Y|I|            Window             |
|       |           |G|K|H|T|N|N|                               |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|           Checksum            |         Urgent Pointer        |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                    Options                    |    Padding    |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
|                             data                              |
+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+

TCP Header Format
*/

#if defined(CONFIG_DEBUG_TCP)
void tcp_dump_header_ingress(sk_buff_t *skb)
{
    iphdr_t  *iph = ip_hdr(skb);
    tcphdr_t *tcph = tcp_hdr(skb);

    /* IP header */
    log_printf("\n\n\nIn:<<<<< === skb ptr %p === <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<\n", skb);
    log_printf("%-8s%hhu.%hhu.%hhu.%hhu->%hhu.%hhu.%hhu.%hhu, protocol %hhu\n", 
           "IP:",
           iph->saddr >> 24, iph->saddr >> 16, iph->saddr >> 8, iph->saddr >> 0,
           iph->daddr >> 24, iph->daddr >> 16, iph->daddr >> 8, iph->daddr >> 0,
           iph->protocol);
    log_printf("%-8sversion %hhu, ihl %hhu, tos 0x%hx, len %hhu\n",
           " ", iph->version, iph->ihl, iph->tos, iph->tot_len);
    log_printf("%-8sid %hhu, frag_off 0x%hx, ttl %hhu, checksum %hu(0x%hx)\n",
           " ", iph->id, ip_offset(iph), iph->ttl, iph->csum, iph->csum);

    /* TCP header */
    log_printf("%-8ssport %hu, port %hu, seq %u, ack %u,\n", 
           "TCP:", tcph->sport, tcph->dport, tcph->seq, tcph->ack_seq);
    log_printf("%-8sreserved %s, offset %hhu, window %hu, checksum %hu,\n",
           " ", "x", tcp_dataoffset(tcph), tcph->win, tcph->csum);
    log_printf("%-8sflags 0x%hhx (%s %s %s %s), urgent data %hu, l4 data len %hu\n",
           " ", tcph->flags, 
           tcp_flags_ack(tcph) ? "ACK" : "",
           tcp_flags_rst(tcph) ? "RST" : "",
           tcp_flags_syn(tcph) ? "SYN" : "",
           tcp_flags_fin(tcph) ? "FIN" : "",
           tcph->urp, tcp_data_len(skb));

    log_printf("TCP option:\n");
    if (tcp_options_len(tcph) < 256) {
        printb((char *)tcp_options(tcph), tcp_options_len(tcph));
    } else {
        log_debug("options in TCP header too long, please check\n");
    }

    /* end of print */
    log_printf("\n");
}

void tcp_dump_header_egress(sk_buff_t *skb, tcphdr_t *tcph)
{
    log_printf("\n\n\nOut:>>>>> === skb ptr %p === >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n", 
               skb);
    /* TCP header */
    log_printf("%-8ssport %hu, port %hu, seq %u, ack %u,\n", 
           "TCP:", tcph->sport, tcph->dport, tcph->seq, tcph->ack_seq);
    log_printf("%-8sreserved %s, offset %hhu, window %hu, checksum %hu,\n",
           " ", "x", tcp_dataoffset(tcph), tcph->win, tcph->csum);
    log_printf("%-8sflags 0x%hhx (%s %s %s %s), urgent data %hu, l4 data len %hu\n",
           " ", tcph->flags, 
           tcp_flags_ack(tcph) ? "ACK" : "",
           tcp_flags_rst(tcph) ? "RST" : "",
           tcp_flags_syn(tcph) ? "SYN" : "",
           tcp_flags_fin(tcph) ? "FIN" : "",
           tcph->urp, (skb->len-tcp_dataoffset(tcph)));
}
#else
void tcp_dump_header_ingress(sk_buff_t *skb){};
void tcp_dump_header_egress(sk_buff_t *skb, tcphdr_t *tcph){};
#endif


static inline void tcp_header_ntoh(sk_buff_t *skb)
{
    tcphdr_t *h = tcp_hdr(skb);

    h->sport = ntohs(h->sport);
    h->dport = ntohs(h->dport);
    h->seq   = ntohl(h->seq);
    h->ack_seq = ntohl(h->ack_seq);
    h->win   = ntohs(h->win);
    h->csum  = ntohs(h->csum);
}

static inline void tcp_header_hton(tcphdr_t *h)
{
    h->sport = htons(h->sport);
    h->dport = htons(h->dport);
    h->seq   = htonl(h->seq);
    h->ack_seq = htonl(h->ack_seq);
    h->win   = htons(h->win);
    h->csum  = 0;  /* will calculate it right after here */
}
    
sk_buff_t * tcp_alloc_skb(uint32_t optlen, uint32_t plen)
{
    uint32_t size = optlen + plen + TCP_HDR_DEFAULT_LEN + IP_HDR_LEN\
                    + ETH_HDR_LEN;

    sk_buff_t *skb = alloc_skb(size);

    skb_reserve(skb, size);
    skb->protocol = PROTO_TCP;
    skb->len = 0; /* increased in every skb_push() */

    return skb;
}

static inline uint32_t tcp_new_iss()
{
    return 0x00001;
}

tcb_t *tcp_alloc_tcb()
{
    tcb_t *tcb = (tcb_t *)malloc(sizeof(tcb_t));
    memset(tcb, 0, sizeof(tcb_t));

    tcb->state = TCP_CLOSED;
    tcb->rcv_wnd = TCP_DEFAULT_WINDOW;
    tcb->iss = tcp_new_iss();
    tcb->mss = TCP_OPT_MSS;

    return tcb;
}

static int tcp_seq_check(tcb_t *tcb, tcphdr_t *tcph)
{
    //uint32_t rcv_end = tcb->rcv_nxt + (tcb->rcv_wnd ?: 1);
    /* RFC 793:
     * Segment Receive  Test
     * Length  Window
     * ------- -------  -------------------------------------------
     *    0       0     SEG.SEQ = RCV.NXT
     *    0      >0     RCV.NXT =< SEG.SEQ < RCV.NXT+RCV.WND
     *   >0       0     not acceptable
     *   >0      >0     RCV.NXT =< SEG.SEQ < RCV.NXT+RCV.WND
     *                  or RCV.NXT =< SEG.SEQ+SEG.LEN-1 < RCV.NXT+RCV.WND
     */
    /* if len == 0, then lastseq == seq */
    /* FIXME */
    return 0;
}

static inline void tcp_dump_state(tcb_t *tcb)
{
    /* state debug information */
    log_debug("tcb state: ");
    if (!tcb)
        log_printf("CLOSED\n");
    else if (tcb->state < TCP_MAX_STATE)
        log_printf("%s\n", tcp_state_string[tcb->state]);
    else
        log_printf("Unknown tcp state: %d\n", tcb->state);
}

static inline void tcp_set_tcb_state(tcb_t *tcb, uint8_t state)
{
    if (state < TCP_MAX_STATE) {
        log_debug("TCP state [ %s ---> %s ]\n", tcp_state_string[tcb->state], 
               tcp_state_string[state]);
        tcb->state = state;
    }
}

tcp_sock_t * tcp_alloc_tcp_sock(iphdr_t *iph, tcphdr_t *tcph)
{
    /* allocate a new tcp_sock_t */
    tcp_sock_t *tcpsk = (tcp_sock_t *)malloc(sizeof(tcp_sock_t));
    memset(tcpsk, 0, sizeof(tcp_sock_t));

    tcpsk->tcb = tcp_alloc_tcb();

    tcpsk->local = iph->daddr;
    tcpsk->peer  = iph->saddr;
    tcpsk->lport = tcph->dport;
    tcpsk->pport = tcph->sport;

    /* add to the list */
    vector_add_last(global_tcp_sock, &tcpsk);
    log_debug("global_tcp_sock size = %d\n", vector_size(global_tcp_sock));

    return tcpsk;
}

tcp_sock_t * tcp_find_tcp_sock(iphdr_t *iph, tcphdr_t *tcph)
{
    /* search on the tcp_sock list */
    tcp_sock_t *tcpsk = NULL;
    int32_t i = 0;
    int32_t size = vector_size(global_tcp_sock);

    for (i = 0; i < size; i++) {
        vector_get_at(&tcpsk, global_tcp_sock, i);

        if ((tcpsk->local == iph->daddr) &&
            (tcpsk->peer == iph->saddr) &&
            (tcpsk->lport == tcph->dport) && 
            (tcpsk->pport == tcph->sport)) {

            return tcpsk;
        }
    }
    /* come to here, means not found, alloc a new tcp_sock */
    log_debug("allocate a new tcp_sock_t\n");
    return tcp_alloc_tcp_sock(iph, tcph);
}

static void tcp_send_ack(tcp_sock_t *tcpsk)
{
    tcphdr_t *out = NULL;
    tcb_t *tcb = tcpsk->tcb;

    sk_buff_t *skb = tcp_alloc_skb(0, 0);

    out = (tcphdr_t *)skb_push(skb, TCP_HDR_DEFAULT_LEN);

    out->sport = tcpsk->lport;
    out->dport = tcpsk->pport;
    out->seq = tcb->snd_nxt;
    out->ack_seq = tcb->rcv_nxt;
    out->offset_rsvd = (5<<4);
    out->flags = FLAG_ACK;
    out->win = tcb->rcv_wnd;

    tcp_send(tcpsk, skb, out);
}

static void tcp_send_fin(tcp_sock_t *tcpsk)
{
    tcphdr_t *out = NULL;
    tcb_t *tcb = tcpsk->tcb;

    sk_buff_t *skb = tcp_alloc_skb(0, 0);

    out = (tcphdr_t *)skb_push(skb, TCP_HDR_DEFAULT_LEN);

    out->sport = tcpsk->lport;
    out->dport = tcpsk->pport;
    out->seq = tcb->snd_nxt;
    out->ack_seq = tcb->rcv_nxt;
    out->offset_rsvd = (5<<4);
    out->flags = FLAG_FIN | FLAG_ACK;
    out->win = tcb->rcv_wnd;

    tcp_send(tcpsk, skb, out);
}

static void tcp_send_syn(tcp_sock_t *tcpsk)
{
    tcphdr_t *out = NULL;
    tcb_t *tcb = tcpsk->tcb;
    uint8_t *opt = NULL;

    sk_buff_t *skb = tcp_alloc_skb(TCP_OPT_MSS_LEN, 0);

    opt = skb_push(skb, TCP_OPT_MSS_LEN);
    opt[0] = 2;
    opt[1] = 4;
    opt[2] = 0x5; opt[3] = 0xb4; /* hard code MSS 1460 */

    out = (tcphdr_t *)skb_push(skb, TCP_HDR_DEFAULT_LEN);

    out->sport = tcpsk->lport;
    out->dport = tcpsk->pport;
    out->seq = tcb->iss;
    out->ack_seq = tcb->rcv_nxt;
    out->offset_rsvd = ((5 + 1)<<4); /* MSS 1 byte */
    out->flags = FLAG_SYN;
    out->win = tcb->rcv_wnd;

    tcp_set_tcb_state(tcb, TCP_SYN_SENT);
    tcb->snd_nxt = tcb->iss + 1;

    tcp_send(tcpsk, skb, out);
}

static void tcp_process_payload(tcp_sock_t *tcpsk, sk_buff_t *skb)
{
    uint8_t *payload = tcp_data(skb);
    uint16_t dlen = tcp_data_len(skb);

    tcb_t *tcb = tcpsk->tcb;
    tcb->rcv_wnd -= dlen;
    tcb->rcv_nxt += dlen;

    write(tcpsk->fd, payload, dlen);

    /* FIXME after write the payload to file, we should update the 
     * receive window, actually, it should not be here.
     * but currently, I just put it here 
     */
    tcb->rcv_wnd += dlen;

    tcp_send_ack(tcpsk);
}

static void tcp_send_reset()
{
}

static void tcp_parse_options(tcp_sock_t *tcpsk, sk_buff_t *skb)
{
    tcphdr_t *tcph = tcp_hdr(skb);
    tcb_t *tcb = tcpsk->tcb;

    uint8_t *opt_start = tcp_options(tcph);
    uint8_t *opt_end = opt_start + tcp_options_len(tcph);
    uint16_t kind = 0, length = 0, meaning = 0;

    log_debug("tcp option length = %d\n", tcp_options_len(tcph));

    while (opt_start != opt_end) {
        kind = opt_start[0];
        length = opt_start[1];
        /* currently only support MSS */
        if (kind == TCP_OPT_KIND_MSS) {
            meaning = opt_start[2] << 8;
            meaning += opt_start[3];
            meaning = ntohs(meaning);

            if (meaning < tcb->mss) {
                tcb->mss = meaning;
            }
            log_debug("tcp option mss = %d\n", tcb->mss);
        } else {
            /* skip option */
            log_debug("skip tcp option kind = %d\n", kind);
        }
        opt_start += length;
    }
}

static void tcp_synsent(tcp_sock_t *tcpsk, sk_buff_t *skb)
{
    tcphdr_t *tcph = tcp_hdr(skb);
    tcb_t *tcb = tcpsk->tcb;

    /* 1) check ACK */
    if (tcp_flags_ack(tcph)) {
        if (tcph->ack_seq <= tcb->iss || tcph->ack_seq > tcb->snd_nxt) {
            tcp_send_reset();
            log_fatal("ack_seq incorrect, drop this packet\n");
            goto drop;
        }
    }

    /* 2) check RST */
    if (tcp_flags_rst(tcph)) {
        log_fatal("RST packet\n");
        tcp_set_tcb_state(tcb, TCP_CLOSED);
        goto drop;
    }

    /* 3) skip security and precedence */
    /* 4) check SYN */
    if (tcp_flags_syn(tcph)) {
        tcb->irs = tcph->seq;
        tcb->rcv_nxt = tcph->seq + 1;
        tcb->snd_wnd = tcph->win;

        if (tcp_options_len(tcph)) {
            tcp_parse_options(tcpsk, skb);
        }

        tcp_set_tcb_state(tcb, TCP_ESTABLISHED);

        tcb->snd_wl1 = tcph->seq;
        tcb->snd_wl2 = tcph->ack_seq;
        /* reply ACK */
        tcp_send_ack(tcpsk);
        log_debug("Active 3-way handshake done\n");

        /* TODO simultaneous open */
    }

drop:
    free_skb(skb);
}

static void tcp_closed(tcp_sock_t *tcpsk, sk_buff_t *skb)
{
    log_debug("tcb is null or tcp closed\n");
    
    if (!skb) {
        tcp_send_reset();
    }
    free_skb(skb);
}

static void tcp_send_synack(tcp_sock_t *tcpsk)
{
    tcphdr_t *out = NULL;
    tcb_t *tcb = tcpsk->tcb;

    sk_buff_t *skb = tcp_alloc_skb(0, 0);

    out = (tcphdr_t *)skb_push(skb, TCP_HDR_DEFAULT_LEN);

    out->sport = tcpsk->lport;
    out->dport = tcpsk->pport;
    out->seq = tcb->iss;
    out->ack_seq = tcb->rcv_nxt;
    out->offset_rsvd = (5<<4);
    out->flags = FLAG_SYN | FLAG_ACK;
    out->win = tcb->rcv_wnd;

    tcp_send(tcpsk, skb, out);
}

static void tcp_listen(tcp_sock_t *tcpsk, sk_buff_t *skb)
{
    tcphdr_t *tcph = tcp_hdr(skb);
    tcb_t *tcb = tcpsk->tcb;

    /* 1) check for RST */
    if (tcp_flags_rst(tcph)) {
        goto drop;
    }
    /* 2) check for ACK */
    if (tcp_flags_ack(tcph)) {
        tcp_send_reset();
        goto drop;
    }
    /* 3) check SYN */
    if (!tcp_flags_syn(tcph)) {
        goto drop;
    }

    tcp_set_tcb_state(tcb, TCP_SYN_RECV);

    tcb->irs = tcph->seq;
    tcb->rcv_nxt = tcph->seq + 1;
    /* send seq = irs, ack = rcv.next, SYN|ACK */
    tcp_send_synack(tcpsk);
    tcb->snd_nxt = tcb->iss + 1;
    tcb->snd_una = tcb->iss;

drop:
    free_skb(skb);
}

static void tcp_segment_arrive(tcp_sock_t *tcpsk, sk_buff_t *skb)
{
    /* tcp header byte-order is already converted in tcp_rcv() */
    tcphdr_t *tcph = tcp_hdr(skb);

    tcb_t *tcb = tcpsk->tcb;
    tcp_dump_state(tcb);

    if (!tcb || tcb->state == TCP_CLOSED) {
        return tcp_closed(tcpsk, skb);
    }
    if (tcb->state == TCP_LISTEN) {
        return tcp_listen(tcpsk, skb);
    }
    if (tcb->state == TCP_SYN_SENT) {
        return tcp_synsent(tcpsk, skb);
    }
    if (tcb->state >= TCP_MAX_STATE) {
        goto drop;
    }

    /* 1) check seq number */
    if (tcp_seq_check(tcb, tcph) < 0) {
        goto drop;
    }
    /* 2) check RST */
    if (tcp_flags_rst(tcph)) {
        /* abort, and notify user */
        switch (tcb->state) {
        case TCP_SYN_RECV:
            break;
        case TCP_ESTABLISHED:
        case TCP_FIN_WAIT1:
        case TCP_FIN_WAIT2:
        case TCP_CLOSE_WAIT:
            /* flush all packets in queue, and notify user */
            break;
        case TCP_CLOSING:
        case TCP_LAST_ACK:
        case TCP_TIME_WAIT:
            break;
        }
        tcp_set_tcb_state(tcb, TCP_CLOSED);
        goto drop;
    }
    /* 3) skip check security and precedence */
    /* 4) check SYN */
    if (tcp_flags_syn(tcph)) {
        /* only LISTEN and SYN-SENT can receive SYN */
        tcp_send_reset();
        tcp_set_tcb_state(tcb, TCP_CLOSED);
    }
    /* 5) check ACK */
    if (!tcp_flags_ack(tcph)) {
        goto drop;
    }
    switch (tcb->state) {
    case TCP_SYN_RECV:
        /* previous state = LISTEN 
         * snd_nxt = iss + 1
         * snd_una = iss 
         *
         * previous state = SYN_SENT
         * snd_nxt = iss + 1
         * snd_una = iss
         */
        if (tcp_flags_ack(tcph)) {
            if (tcb->snd_una <= tcph->ack_seq && tcph->ack_seq <= tcb->snd_nxt) {
                log_debug("Passive 3-Way handshake done \n");
                /* TODO  notify user ? */
            } else {
                tcp_send_reset();
                goto drop;
            }
            tcp_set_tcb_state(tcb, TCP_ESTABLISHED);
        }
        /* TODO: if timer expired, send RST */
        if (tcp_flags_fin(tcph) || tcp_flags_rst(tcph)) {
            tcp_set_tcb_state(tcb, TCP_LISTEN);
            goto drop;
        }
        break;
    case TCP_ESTABLISHED:
    case TCP_CLOSE_WAIT:
    case TCP_LAST_ACK:
    case TCP_FIN_WAIT1:
    case TCP_CLOSING:
        break;
        /* TODO */
    }

    /* 6) skip check URG */
    /* 7) get the payload */
    switch (tcb->state) {
    case TCP_ESTABLISHED:
    case TCP_FIN_WAIT1:
    case TCP_FIN_WAIT2:
        tcp_process_payload(tcpsk, skb);
        break;
        /* other cases, payload is ignored */
    }
    /* 8) check FIN */
    if (tcp_flags_fin(tcph)) {
        switch (tcb->state) {
        case TCP_SYN_RECV:
        case TCP_ESTABLISHED:
            tcp_set_tcb_state(tcb, TCP_CLOSE_WAIT);
            /* FIN consumes 1 seq num */
            tcb->rcv_nxt += 1;
            tcp_send_ack(tcpsk);

            //tcp_send_fin(tcpsk);
            break;
        case TCP_FIN_WAIT1:
            tcp_set_tcb_state(tcb, TCP_CLOSING);
            break;
        case TCP_CLOSE_WAIT:
        case TCP_CLOSING:
        case TCP_LAST_ACK:
            break;
        case TCP_FIN_WAIT2:
            break;
        }
    }

drop:
    free_skb(skb);
}

#define TCP_DUMP_PAYLOAD 0

void tcp_rcv(sk_buff_t *skb)
{
    tcp_header_ntoh(skb);
    tcp_dump_header_ingress(skb);

#if TCP_DUMP_PAYLOAD
    printb((char *)tcp_data(skb), tcp_data_len(skb));
#endif

    tcp_sock_t *sock = NULL;

    iphdr_t *iph = ip_hdr(skb);
    tcphdr_t *tcph = tcp_hdr(skb);

    /* add 4 tuple to tcp_sock */
    sock = tcp_find_tcp_sock(iph, tcph);

    /* Follow RFC 793, Segment Arrives */
    tcp_segment_arrive(sock, skb);
}

void tcp_send(tcp_sock_t *tcpsk, sk_buff_t *skb, tcphdr_t *tcph)
{
    /* attach tcp_sock to this skb */
    skb->sock = tcpsk;

    tcp_dump_header_egress(skb, tcph);

    /* transfer to network byte order */
    tcp_header_hton(tcph);

    /* calculate tcp header checksum 
     * Because IP header now is empty, to get the IP payload len, 
     * use skb->len
     */
    tcph->csum = tcp_checksum(htonl(tcpsk->local), htonl(tcpsk->peer), 
                              IP_TCP, (skb->len & 0xffff), (uint8_t*)tcph);

    /* give this skb to IP layer */
    ip_output(skb, skb->sock->peer);
}

/* utcp APIs */
void utcp_listen()
{
    iphdr_t iph;
    tcphdr_t tcph;

    iph.saddr = 0xa000005;
    iph.daddr = 0xa000004;
    tcph.sport = 4321;
    tcph.dport = 1234;

    tcp_sock_t *tcpsk = tcp_find_tcp_sock(&iph, &tcph);
    tcp_set_tcb_state(tcpsk->tcb, TCP_LISTEN);
    
    tcpsk->fd = open("/tmp/save.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (tcpsk->fd < 0) {
        log_debug("open file failed, errno = %d\n", errno);
    }
}

void utcp_socket()
{
}

void utcp_bind()
{
}

void utcp_accept()
{
}

void utcp_close(tcp_sock_t *tcpsk)
{
    tcp_send_fin(tcpsk);
}

void utcp_connect(uint32_t peer, uint16_t pport)
{
    iphdr_t iph;
    tcphdr_t tcph;

    iph.saddr = peer;
    iph.daddr = 0xa000004;
    tcph.sport = pport;
    tcph.dport = 1234;

    tcp_sock_t *tcpsk = tcp_find_tcp_sock(&iph, &tcph);
    if (!tcpsk) {
        printf("Fatal error, exit\n");
    }
    
    tcpsk->fd = open("/tmp/save.txt", O_WRONLY | O_CREAT | O_TRUNC, 0644);
    if (tcpsk->fd < 0) {
        log_debug("open file failed, errno = %d\n", errno);
    }

    tcp_send_syn(tcpsk);
}
