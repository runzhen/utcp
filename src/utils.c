#include "syshead.h"
#include "utils.h"
#include <ctype.h>

void printb(char *buf, uint32_t size)
{
    char l[80];
    char *t = l + 10;
    char *s = t + 52;
    uint32_t i = 0, j = 0;

    memset(t, ' ', s - t);
    snprintf((char *)l, sizeof(l), "%08x: ", 0);

    for (i = 0, j = 0; i < size; i++) {
        snprintf((char *)t, sizeof(char *), "%02x ", buf[i]);
        if (isprint(buf[i])) {
            *s = buf[i];
        } else {
            *s = '.';
        }
        ++j;
        t += 3;
        s++;
        if (j == 8) {
            *t++ = ' ';
            *s++ = ' ';
        } else if (j == 16) {
            *t = ' ';
            *s = 0;
            log_printf("%s\n", l);
            t = l + 10;
            s = t + 52;
            memset(t, ' ', s - t);
            j = 0;
            snprintf((char *)l, sizeof(l), "%08x: ", i + 1);
        }
    }
    if (j != 0) {
        *t = ' ';
        *s = 0;
        log_printf("%s\n", l);
    }
}

int run_cmd(char *cmd, ...)
{
    va_list ap;
    char buf[CMDBUFLEN];
    va_start(ap, cmd);
    vsnprintf(buf, CMDBUFLEN, cmd, ap);

    va_end(ap);

    return system(buf);
}

uint32_t sum_every_16bits(void *addr, uint16_t count)
{
    uint32_t sum = 0;
    uint16_t *ptr = addr;
    
    while (count > 1) {
        /*  This is the inner loop */
        sum += *ptr;
        ptr++;
        count -= 2;
    }

    /*  Add left-over byte, if any */
    if (count > 0)
        sum += * (uint8_t *)ptr;

    return sum;
}

uint16_t checksum(void *addr, uint16_t count, uint32_t start_sum)
{
    /* Compute Internet Checksum for "count" bytes
     *         beginning at location "addr".
     * Taken from https://tools.ietf.org/html/rfc1071
     */
    uint32_t sum = start_sum;

    sum += sum_every_16bits(addr, count);
    
    /*  Fold 32-bit sum to 16 bits */
    while (sum >> 16)
        sum = (sum & 0xffff) + (sum >> 16);

    return ~sum;
}

/*  TCP checksum functions */
uint16_t tcp_checksum(uint32_t src, uint32_t dst, uint16_t proto, 
                      uint16_t len, uint8_t *data)
{
    uint32_t sum = 0;

    sum = htons(proto) + htons(len);

    sum += (src & 0xFFFF);
    sum += (src >> 16);

    sum += (dst & 0xFFFF);
    sum += (dst >> 16);

    return checksum(data, len, sum);
}

int get_address(char *host, char *port, struct sockaddr *addr)
{
    struct addrinfo hints;
    struct addrinfo *result, *rp;
    int s;

    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;

    s = getaddrinfo(host, port, &hints, &result);

    if (s != 0) {
        print_err("getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    for (rp = result; rp != NULL; rp = rp->ai_next) {
        *addr = *rp->ai_addr;
        freeaddrinfo(result);
        return 0;
    }
    
    return 1;
}

uint32_t parse_ipv4_string(char* addr) {
    uint8_t addr_bytes[4];
    sscanf(addr, "%hhu.%hhu.%hhu.%hhu", &addr_bytes[3], &addr_bytes[2], &addr_bytes[1], &addr_bytes[0]);
    return addr_bytes[0] | addr_bytes[1] << 8 | addr_bytes[2] << 16 | addr_bytes[3] << 24;
}

uint32_t min(uint32_t x, uint32_t y) {
    return x > y ? y : x;
}
