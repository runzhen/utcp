#include "syshead.h"
#include "skbuff.h"
#include "netdev.h"
#include "list.h"

/* caller should garantee the size is enough.
 * e.x. TCP wrapper, size = payload + TCP header + IP + ETH
 * */
sk_buff_t *alloc_skb(unsigned int size)
{
    sk_buff_t *skb = malloc(sizeof(sk_buff_t));

    memset(skb, 0, sizeof(sk_buff_t));
    skb->data = malloc(size);
    memset(skb->data, 0, size);

    skb->refcnt = 0;
    skb->head = skb->data;
    skb->end = skb->data + size;
    skb->dev = netdev_get(0xbeef);

    list_init(&skb->list);

    return skb;
}

void free_skb(sk_buff_t *skb)
{
    if (skb && skb->refcnt < 1) {
        free(skb->head);
        free(skb);
    }
}

void *skb_reserve(struct sk_buff *skb, unsigned int len)
{
    skb->data += len;
    return skb->data;
}

uint8_t *skb_push(struct sk_buff *skb, unsigned int len)
{
    skb->data -= len;
    skb->len += len;

    return skb->data;
}

uint8_t *skb_head(sk_buff_t *skb)
{
    return skb->head;
}

void skb_reset_len(sk_buff_t *skb)
{
    skb->len = 0;
}

void skb_len_set(sk_buff_t *skb, unsigned int len)
{
    skb->len = len;
}
