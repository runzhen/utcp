#!/usr/bin/python

import random
from scapy.all import *
dip = "10.0.0.4"

payload = "A"*496+"B"*500
packet = IP(dst=dip, id=12345)/UDP(sport=1500,dport=1501)/payload

frags = fragment(packet, fragsize=100)


def sequen_frag():
    counter = 1
    for f in frags:
        print "Packet no#" + str(counter)
        print "==================================================="
        #f.show() #displays each fragment
        counter += 1
        send(f)

def random_frag():
    counter = 1
    print "total " + str(len(frags)) + " frags"
    send(frags[0])
    send(frags[9])
    send(frags[3])
    send(frags[2])
    send(frags[1])
    send(frags[6])
    send(frags[7])
    send(frags[8])
    send(frags[5])
    send(frags[4])



random_frag()
