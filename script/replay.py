#!/usr/bin/python

from scapy.all import *
import  os
import  sys

filename = "text1.pcap"

def send_single_packet(idx):
    pcap = rdpcap(filename)

    packet = pcap[idx]

    #packet[IP].dst = "10.0.0.4"
    sendp(packet, iface="tap0")


def send_all():
    pcap = rdpcap(filename)

    for i in range(len(pcap)):
        sendp(pcap[i], iface="tap0")



if __name__ == '__main__':

    if os.path.isfile(filename) == False:
        print("%s doesn't exist\n", sys.argv[1])

    if len(sys.argv) == 2:
        send_single_packet(int(sys.argv[1]))

    if len(sys.argv) == 1:
        send_all()
