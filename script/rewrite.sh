#!/bin/bash

if [ $# -lt 2 ];
then
	echo ""
	echo "Usage replay <src-ip> <dst-ip>"
	echo ""
	exit
fi

int2="ens192"
int1="ens224"
stats=30	;# how often to show stats

src=$1
dst=$2

pcap="text1.pcap"
cache="text1.cache"
outf="tmp.pcap"

# first create cache files for each .pcap file in the directory for bi-directional traffic
echo creating cache file for $pcap
tcpprep --auto=bridge --pcap=$pcap --cachefile=$cache
echo created cache file $cache

tcprewrite  --endpoints="$dst:$src"   --cachefile=$cache --infile=$pcap --outfile=$outf

date
#echo replaying...
#tcpreplay -p 10 -i $int1 -I $int2 -c $cache tmp.pcap

rm -f $cache
mv $outf $pcap
